 [![pipeline status](https://gitlab.pasteur.fr/tru/miniconda3-python310-pytorch-cuda11.7-nightly/badges/main/pipeline.svg)](https://gitlab.pasteur.fr/tru/miniconda3-python310-pytorch-cuda11.7-nightly/-/commits/main) 

 # miniconda3 based for python 3.10 and pytorch nightly, pytorch-cuda=11.7
 Alternative to the regular command listed at https://pytorch.org/get-started/locally/
 
 `conda install pytorch torchvision torchaudio pytorch-cuda=11.7 -c pytorch-nightly -c nvidia`

 # Goals
 - smaller foot print for python3.10/pytorch gpu/cuda 11.7
 
 # removed on purpose from a plain installation
 
 `conda create -n py310-pytorch python=3.10 && conda activate py310-pytorch && conda install pytorch pytorch-cuda=11.7 -c pytorch-nightly -c nvidia -y`
- cuda-nsight
- cuda-nsight-compute
- nsight-compute
- cuda-demo-suite

 # usage

docker:
```
docker run -ti docker://registry-gitlab.pasteur.fr/tru/miniconda3-python310-pytorch-cuda11.7-nightly:main
```

singularity/apptainer:
```
singularity build miniconda3-python310-pytorch-cuda11.7.sif oras://registry-gitlab.pasteur.fr/tru/miniconda3-python310-pytorch-cuda11.7-nightly:latest
```
